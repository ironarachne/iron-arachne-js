export default class WeaponComponent {
  name: string;
  options: string[];

  constructor(name: string, options: string[]) {
    this.name = name;
    this.options = options;
  }
}
