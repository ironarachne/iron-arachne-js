export default class WeaponEffect {
  name: string;
  options: string[];

  constructor(name: string, options: string[]) {
    this.name = name;
    this.options = options;
  }
}
