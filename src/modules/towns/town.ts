export default class Town {
  name: string;
  description: string;

  constructor(name: string) {
    this.name = name;
    this.description = "";
  }
}
